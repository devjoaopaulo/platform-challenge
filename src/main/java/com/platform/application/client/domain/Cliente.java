package com.platform.application.client.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nome;
    @Column(name="CPF", unique = true)
    private String cpf;
    private LocalDate dataNascimento;

    public Integer getIdade() {
        return Period.between(dataNascimento, LocalDate.now()).getYears();
    }

}
