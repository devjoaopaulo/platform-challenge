package com.platform.application.client.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface IRestController<T, ID, DTO> {

    DTO findById(ID id);

    Page<DTO> find(Map<String, Object> params, Pageable pageable);

    void deleteById(ID id);

    DTO save(DTO dto);

    DTO update(DTO dto, ID id);

    DTO updatePartial(Map<String, Object> params, ID id);
}