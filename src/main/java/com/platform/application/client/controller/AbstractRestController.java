package com.platform.application.client.controller;

import com.platform.application.client.mapper.IMapper;
import com.platform.application.client.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;


public abstract class AbstractRestController<T, ID, DTO> implements IRestController<T, ID, DTO> {

    @Autowired
    private IService<T, ID> service;

    @Autowired
    private IMapper<T,DTO> mapper;

    @GetMapping
    @Override
    @ResponseStatus(HttpStatus.OK)
    public Page<DTO> find(@RequestParam(required = false) final Map<String, Object> params,
    @PageableDefault(size = 20)
    @SortDefault.SortDefaults({
            @SortDefault(sort = "nome", direction = Sort.Direction.ASC),
            @SortDefault(sort = "id", direction = Sort.Direction.ASC)
    }) Pageable pageable) {
        if (params.isEmpty()){
            return this.service.findAllWithPagination(pageable).map(mapper::toDTO);
        }
        return this.service.findByObjectWithPagination(params, pageable).map(mapper::toDTO);
    }


    @GetMapping("/{id}")
    @CrossOrigin
    @Override
    @ResponseStatus(HttpStatus.OK)
    public DTO findById(@PathVariable final ID id) {
        return mapper.toDTO(this.service.findById(id));
    }


    @Override
    @CrossOrigin
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable final ID id) {
        this.service.deleteById(id);
    }

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DTO save(@Valid @RequestBody final DTO dto) {
        final T obj = this.service.save(mapper.fromDTO(dto));
        return mapper.toDTO(obj);
    }

    @Override
    @CrossOrigin
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public DTO update(@Valid @RequestBody final DTO dto, @PathVariable final ID id) {
        final T obj = this.service.update(mapper.fromDTO(dto),id);
        return mapper.toDTO(obj);
    }

    @Override
    @CrossOrigin
    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public DTO updatePartial(@Valid @RequestBody final Map<String, Object> map, @PathVariable final ID id) {
        final T obj = this.service.updatePartial(map,id);
        return mapper.toDTO(obj);
    }

}
