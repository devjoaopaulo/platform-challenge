package com.platform.application.client.controller;

import com.platform.application.client.constants.Path;
import com.platform.application.client.domain.Cliente;
import com.platform.application.client.mapper.ClienteDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

@RestController
@RequestMapping(Path.CLIENTE)
@RequestScope
public class ClienteRestController extends AbstractRestController<Cliente,Integer, ClienteDTO> {
}
