package com.platform.application.client.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface IService<T, ID> {

   T findById(ID id);

   Page<T> findAllWithPagination(Pageable pageable);

   void deleteById(ID id);

   T save(T t);

   T update(T t, ID id);

   Page<T> findByObjectWithPagination(Map<String, Object> params, Pageable pageable);

   T updatePartial(Map<String, Object> params,ID id);

}
