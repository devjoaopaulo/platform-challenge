package com.platform.application.client.utils;

import com.platform.application.client.exceptions.BusinessException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Assert {

    /*
     * IsTrue and IsFalse
     */
    public static boolean isTrue(boolean condition, String message) {
        if (!condition) {
            throw new BusinessException(message);
        }
        return true;
    }
}
