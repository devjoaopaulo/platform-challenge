package com.platform.application.client.mapper;

import com.platform.application.client.validator.AlphaSpace;
import com.platform.application.client.validator.ForeignExists;
import com.platform.application.client.validator.ValidDate;
import com.platform.application.client.validator.ValueOfEnum;
import lombok.*;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClienteDTO {

    private Integer id;

    @NotBlank
    @AlphaSpace(message = "name must not contain special characters or numbers")
    private String nome;

    @CPF
    private String cpf;

    @NotNull
    @ValidDate(format = "yyyy-MM-dd", message = "Invalid format date, expect: yyyy-MM-dd")
    private String dataNascimento;

    private Integer idade;
}
