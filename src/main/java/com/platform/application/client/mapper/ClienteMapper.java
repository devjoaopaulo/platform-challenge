package com.platform.application.client.mapper;

import com.platform.application.client.domain.Cliente;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ClienteMapper extends IMapper<Cliente,ClienteDTO> {
    @Override
    @Mapping(source = "dataNascimento", target = "dataNascimento", dateFormat = "yyyy-MM-dd")
    ClienteDTO toDTO(Cliente cliente);

    @Override
    @Mapping(source = "dataNascimento", target = "dataNascimento", dateFormat = "yyyy-MM-dd")
    Cliente fromDTO(ClienteDTO clienteDTO);

    @Override
    List<ClienteDTO> toDTO(List<Cliente> t);

    @Override
    List<Cliente> fromDTO(List<ClienteDTO> dto);
}
