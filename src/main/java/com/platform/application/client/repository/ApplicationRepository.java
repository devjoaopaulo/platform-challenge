package com.platform.application.client.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

@NoRepositoryBean
public interface ApplicationRepository<T, ID> extends JpaRepository<T,ID>, PagingAndSortingRepository<T,ID> {
}
