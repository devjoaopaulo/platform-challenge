package com.platform.application.client.controller;

import com.platform.application.client.constants.Path;
import com.platform.application.client.controller.controller.CrudRestTest;
import com.platform.application.client.mapper.ClienteDTO;
import com.platform.application.client.utils.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ClienteRestControllerTest extends CrudRestTest<ClienteDTO,Integer> {

    @Before
    public void setUp(){
        super.setUp();
    }

    @Override
    protected ClienteDTO createObject() {
        return ClienteDTO.builder()
                .cpf(new TestUtils().gerarCPF())
                .dataNascimento(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDate.of(1996,5,20)))
                .nome("Luciano Barreto")
                .build();
    }

    @Override
    protected ClienteDTO setObject(ClienteDTO clienteDTO) {
        clienteDTO.setNome("Marcelo Moura");
        return clienteDTO;
    }

    @Override
    protected String getPath() {
        return Path.CLIENTE;
    }

    @Override
    protected UriComponentsBuilder getUriWithRequestParams() {
        return uriComponentsBuilder
                .queryParam("nome","Luciano");
    }


    @Test
    public void create() throws Exception {
        super.create();
    }

    @Test
    public void update() throws Exception {
        super.update();
    }

    @Test
    public void delete() throws Exception {
        super.delete();
    }

    @Test
    public void read() throws Exception {
        super.readAsPage();
    }

    @Test
    public void findById() throws Exception {
        super.findById();
    }

    @Test
    public void findByRequestParam() throws Exception {
        super.findByRequestParamWithPagination();
    }

}
