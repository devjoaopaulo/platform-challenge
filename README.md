# Platform Builders Cliente

## API
- to test the application you can use the postman backup that is inside the postman folder or you can access the swagger UI. 

## Requirements
- Docker 
- Docker Compose
- JDK 1.8

## Getting started
- ./mvnw clean install && docker-compose up

## Swagger-UI
- /swagger-ui.html
